from flask import Flask, jsonify, request
from controllers import user
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
app.config['JWT_SECRET_KEY'] = "testkunci"
jwt = JWTManager(app)

# route API yang digunakan untuk melihat semua data user
@app.route("/users", methods=["GET"])
def usersService():
    try:
        return user.viewUsers()
    except Exception as e:
        message = {
            "message": "kesalahan function userService",
            "error" : e
        }
        return jsonify(message)


# route API yang digunakan untuk melihat data by id_user dan menambah data
@app.route("/user", methods=["GET"])
def userService():
    try:
        params = request.json
        return user.viewUserById(**params)
    except Exception as e:
        message = {
            "message": "kesalahan function userService",
            "error" : e
        }
        return jsonify(message)


# route APi yang digunakan untuk menambah data user
@app.route("/user/insert", methods=["POST"])
def insertUserService():
    try:
        params = request.json
        return user.tambahData(**params)
    except Exception as e:
        message = {
            "message": "kesalahan function inserUserService",
            "error" : e
        }
        return jsonify(message)


# route API yang digunakan untuk mengubah data user
@app.route("/user/update", methods=["POST"])
def updateUserService():
    try:
        params = request.json
        return user.ubahDataById(**params)
    except Exception as e:
        message = {
            "message": "kesalahan fucntion updateUserService",
            "error" : e
        }
        return jsonify(message)


# route API yang digunakan untuk menghapus data user berdasararkan id_user
@app.route("/user/delete", methods=["POST"])
def deleteUserService():
    try:
        params = request.json
        return user.hapusDataById(**params)
    except Exception as e:
        message = {
            "message" : "kesalahan fucntion deleteUserService",
            "error" : e
        }
        return jsonify(message)


# route API yang digunakan untuk mendapatkan bearer token
@app.route("/token", methods=["POST"])
def token():
    try:
        params = request.json
        return user.getToken(**params)
    except Exception as e:
        message = {
            "message" : "error function token",
            "error" : e
        }
        return jsonify(message)


if __name__ == "__main__":
    app.run(debug=True, port=5000)
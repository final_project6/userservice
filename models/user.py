from sqlalchemy import create_engine, Column, String, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    __tablename__ = "user"
    id_user = Column(Integer, primary_key=True)
    nama_user = Column(String)
    email_user = Column(String)
    alamat_user = Column(String)
    no_hp_user = Column(String)

class Database:
    # method yang digunakan untuk melakukan koneksi database
    def __init__(self):
        try:
            self.engine = create_engine("mysql+mysqlconnector://root:@localhost:3306/jualmobil", echo=True)
            self.Session = sessionmaker(bind=self.engine)
            self.session = self.Session()
            print("sukses koneksi")
        except Exception as e:
            print(f"gagal koneksi database: {e}")

    # method yang digunakan untuk melihat semua data dalam tabel user
    def showUsers(self):
        try:
            hasil = self.session.query(User).all()  #tampung dalam variabel hasil
            return hasil  
        except Exception as e:
            print(f"kesalahan function showUser: {e}")

    # method yang digunakan untuk melihat data user berdasarkan id_user
    def showUserById(self, **params):
        try:
            hasil = self.session.query(User).filter(User.id_user == params["id_user"]).one()
            return hasil
        except Exception as e:
            print(f"kesalahan fucntion showUserById: {e}")

    
    # method yang digunakan untuk menambah data ke tabel user
    def insertUser(self, **params):
        try:
            self.session.add(User(**params))  # tambah data
            self.session.commit()  # memberitahukan ke server
        except Exception as e:
            print(f"kesalahan function insertuser: {e}")

    # method yang digunakan untuk mengubah data user
    def updateDataById(self, **params):
        try:
            dataUser = self.session.query(User).filter(User.id_user == params["id_user"]).one()  # ambil data user by id_user
            if "nama_user" in params.keys():
                dataUser.nama_user = params["nama_user"]
            if "no_hp_user" in params.keys():
                dataUser.no_hp_user = params["no_hp_user"]
            if "email_user" in params.keys():
                dataUser.email_user = params["email_user"]
            if "alamat_user" in params.keys():
                dataUser.alamat_user = params["alamat_user"]
            self.session.commit()  # memberitahu server
        except Exception as e:
            print(f"kesalahan function updateDataById: {e}")


    # method yang digunakan untuk menghapus data user by id_user
    def deleteUserById(self, **params):
        try:
            dataUser = self.session.query(User).filter(User.id_user == params["id_user"]).one()
            self.session.delete(dataUser)  # hapus data
            self.session.commit()  # memberitahu server
        except Exception as e:
            print(f"kesalahan fucntion deleteuserById: {e}")

    
    # method yang digunakan untuk melihat by email_user -> untuk JWT
    def showUserByEmail(self, **params):
        try:
            hasil = self.session.query(User).filter(User.email_user == params["email_user"]).one()
            return hasil
        except Exception as e:
            print(f"kesalahan function showUserByEmail: {e}")
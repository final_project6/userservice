# userService
 berisi aplikasi yang menyediakan service untuk user seperti create data user, read data user, update data user, dan delete data user.<br>
 Aplikasi ini adalah aplikasi jual beli mobil sederhana, terdapat 3 service userService untuk menampung data user, sellerService untuk menampung data mobil yang dijual, dan transaksiService yang digunakan untuk menampung data transaksi yang terjadi.

## Cara Menjalankan
masuk ke direktori file lalu ketikkan `python app.py` pada command prompt, akan berjalan pada port 5000

## Routing API
### Mendapatkan Bearrer Token
`http://localhost:5000/token`

data = { <br>
    "email_user" : "reoshby@gmail.com" <br>
}

### Create data user
`http://localhost:5000/user/insert` method POST

data = {<br>
    "nama_user": "Reo Sahobby", <br>
    "email_user" : "reoshby@gmail.com", <br>
    "alamat_user" : "Klaten", <br>
    "no_hp_user" : "083863865819"<br>
}

### Show All User
`http://localhost:5000/users` method GET

### Show User By Id
`http://localhost:5000/user` method GET

data = {<br>
    "id_user" : 6 <br>
}

### Update Data User
`http://localhost:5000/user/update` method POST

data = { <br>
    "id_user" : 6, <br>
    #... key - value yang akan di update ...# <br>
}

### Delete Data User
`http://localhost:5000/user/delete` method POST

data = {<br>
    "id_user" : 2 <br>
}

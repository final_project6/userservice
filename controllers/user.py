from models.user import Database
from flask import jsonify
from flask_jwt_extended import *
import datetime

db = Database()

# function untuk melihat semua data di tabel user
@jwt_required()
def viewUsers():
    try:
        hasil = db.showUsers()
        data_list = []
        for data in hasil:
            user = {
                "id_user" : data.id_user,
                "nama_user" : data.nama_user,
                "email_user" : data.email_user,
                "alamat_user" : data.alamat_user,
                "no_hp_user" : data.no_hp_user
            }
            data_list.append(user)
        return jsonify(data_list)
    except Exception as e:
        print(f"kesalahan function viewUsers: {e}")


# function untuk melihat satu data user berdasarkan id_user
def viewUserById(**params):
    try:
        hasil = db.showUserById(**params)
        data = {
            "id_user" : hasil.id_user,
            "nama_user" : hasil.nama_user,
            "email_user" : hasil.email_user,
            "alamat_user" : hasil.alamat_user,
            "no_hp_user" : hasil.no_hp_user,
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function viewusersById: {e}")


# function untuk menambah data ke tabel user
def tambahData(**params):
    try:
        db.insertUser(**params)
        data = {
            "message" : "data berhasil ditambahkan"
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function tambahData: {e}")


# function untuk mengubah data user
def ubahDataById(**params):
    try:
        db.updateDataById(**params)
        data = {
            "message" : "data berhasil diubah"
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function ubahData: {e}")


# function untuk menghapus data by id_user
def hapusDataById(**params):
    try:
        db.deleteUserById(**params)
        data = {
            "message" : "data berhasil dihapus"
        }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan function hapusDataById: {e}")


# function untuk mendapatkan token
def getToken(**params):
    try:
        dataUser = db.showUserByEmail(**params)
        if dataUser is not None:
            user = {
                "id_user" : dataUser.id_user,
                "email" : dataUser.email_user
            }
            expires = datetime.timedelta(days=1)
            token = create_access_token(user, fresh=True, expires_delta=expires)
            data = {
                "data": user,
                "token": token
            }
        else:
            data = {
                "message": "email tidak terdaftar"
            }
        return jsonify(data)
    except Exception as e:
        print(f"kesalahan fucntion getToken: {e}")